#Patrones de diseño

Los **patrones de diseño** (design patterns) son soluciones
habituales a problemas comunes en el diseño de
software. Cada patrón es como un plano que se
puede personalizar para resolver un problema de
diseño particular de tu código.
        
##Ventajas de los patrones

Los patrones son un juego de herramientas que
brindan soluciones a problemas habituales
en el diseño de software. Definen un
lenguaje común que ayuda a tu
equipo a comunicarse
con más eficiencia.
        
##Clasificación

Los patrones de diseño varían en su complejidad,
nivel de detalle y escala de aplicabilidad.
Además, pueden clasificarse por su
propósito y dividirse en tres grupos.
        
##Historia de los patrones

¿Quién inventó los patrones y cuándo?
¿Se pueden utilizar los patrones fuera del
desarrollo de software? ¿Cómo se hace?
        
##Crítica de los patrones

¿Son tan buenos los patrones como se dice?
¿Es siempre posible utilizarlos?
¿Pueden los patrones ser dañinos en alguna ocasión

##Catálogo de patrones

Lista de 22 patrones de diseño clásicos,
agrupados con base en su propósito.